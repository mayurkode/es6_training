class Person {
    name;
    age;

    constructor(name, age) {
        this.age = age;
        this.name = name;
    }

    greet() {
        return `${this.name} is ${this.age} year old.`
    }

    getName = () => this.name.toUpperCase();
    getAge = () => this.age;
}

class Developer extends Person {
    skill;
    constructor(name, age, skill) {
        super(name, age)
        this.skill = skill;
    }

    hasSkills() {
        return this.skill;
    }

    greet() {
        super.greet();
        return 'and I have ' + this.skill;
    }
}

export {
    Person,
    Developer
}