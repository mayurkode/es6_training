import {
    name,
    greet
} from './utils.js';
import language from './utils.js';
import * as maths from './math.js';
import {
    Person, Developer
} from './Person.js';

import { Helper } from './helper.js'
import * as listSymbols from './symbol.js';


console.log(name);
greet();

console.log(language);

console.log(maths.add(3, 4));

let mayur = new Person('mayur', 30);

console.log(mayur.greet());



let dev = new Developer('Mayur',30,'UI');

console.log(dev.greet());
console.log('works on', dev.hasSkills());

Helper.getYear();


console.log(mayur.getName());

console.log('tyep of two:',typeof listSymbols.dell)

console.log('sym' , listSymbols.dell);
console.log('same ? ', listSymbols.dell == listSymbols.hp);

console.log('same ? ', listSymbols.one == listSymbols.two);

console.log(listSymbols.maruti.wheel);