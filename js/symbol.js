const dell = Symbol('dell')

const hp = Symbol('hp');

const one = Symbol.for('dell')

const two = Symbol.for('dell');


export class Car {
    wheel = 2;
    [dell] = 3;
}

let maruti = new Car();
export { dell, hp, one, two, maruti }; 